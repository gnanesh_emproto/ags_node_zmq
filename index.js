// Weather update server in node.js
// Binds PUB socket to tcp://*:5556
// Publishes random weather updates
const zmq = require("zeromq");
const subscribeSocket = new zmq.Subscriber();
const publisherSocket = new zmq.Publisher();


async function run() {  
  subscribeSocket.connect("tcp://127.0.0.1:8822");
  subscribeSocket.subscribe("");

  publisherSocket.bind("tcp://*:8811");
  //publisher.bindSync("ipc://weather.ipc");

  //while (true) {
    // Get values that will fool the boss
    var update = "command from nodejs";
    triggerListener();
    setTimeout(() => {
      console.log(" DISPENSER_STATUS_CMD => ", update);
      publisherSocket.send(update);
    }, 500);

    
  //}
}

async function triggerListener() {
  for await (const [msg] of subscribeSocket) {
    //console.log(topic);
    //console.log(msg);
    const res = JSON.parse(msg.toString());

    console.log("Message from driver: ", res);

    if (res.type === "OK") {
      console.log("");
      console.log(JSON.parse(msg.toString()));
      console.log("");
    }
  }
}
run();